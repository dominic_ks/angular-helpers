import { TestBed } from '@angular/core/testing';

import { GlobalCommsService } from './global-comms.service';

describe('GlobalCommsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GlobalCommsService = TestBed.get(GlobalCommsService);
    expect(service).toBeTruthy();
  });
});
