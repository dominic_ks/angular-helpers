import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { GlobalAction } from '../../classes/global-action/global-action';

@Injectable({
  providedIn: 'root'
})
export class GlobalCommsService {

  private globalActionSubject = new BehaviorSubject<GlobalAction>({
    action: 'clear',
  });
  public globalAction = this.globalActionSubject.asObservable();

  constructor() { }

  broadcastGlobalAction( action: GlobalAction ): void {
    this.globalActionSubject.next( action );
    this.clearGlobalAction();
  }

  clearGlobalAction(): void {
    this.globalActionSubject.next({
      action: 'clear',
    });
  }

}
