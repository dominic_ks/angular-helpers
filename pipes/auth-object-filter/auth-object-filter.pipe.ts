import { Pipe, PipeTransform } from '@angular/core';

export class filterObject {
  loginStatus: string[];
  allowedRoles: string[];
}

export class filterRule {
  roles: string[];
}

@Pipe({
  name: 'authObjectFilter'
})
export class AuthObjectFilterPipe implements PipeTransform {

  transform( objects: filterObject[] , filterRules: filterRule , loginStatus: string ): filterObject[] {

    let returnObjects: filterObject[] = []

    for( let object of objects ) {
      if( this.checkLoginStatus( object , loginStatus )) {
        returnObjects.push( object );
      }
    }

    return returnObjects;

  }

  checkLoginStatus( object: filterObject , loginStatus: string ): boolean {

    let objectRequirements = object.loginStatus;

    if( objectRequirements.length !== 0 && loginStatus === 'unknown' ) {
      return false;
    }

    if( objectRequirements.indexOf( 'logged-in' ) !== -1 && loginStatus !== 'logged-in' ) {
      return false;
    }

    if( objectRequirements.indexOf( 'not-logged-in' ) !== -1 && loginStatus !== 'not-logged-in' ) {
      return false;
    }

    return true;

  }

}
