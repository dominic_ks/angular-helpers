import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'objectFilter',
  pure: false,
})
export class ObjectFilterPipe implements PipeTransform {

  transform( objects: Object[] , args: Object ): any {

    for( let key in args ) {
      let filterValue = args[ key ];
      objects = this.filterObjects( objects , key , filterValue );
    }

    return objects;

  }

  filterObjects( objects: Object[] , key: any , value: any ): Object[] {

    let returnObjects = []

    for( let item of objects ) {
      if( this.filterObject( item , key , value )) {
        returnObjects.push( item );
      }
    }

    return returnObjects;

  }

  filterObject( object: Object , key: any , value: any ): boolean {
    return ( typeof( object[ key ] ) !== 'undefined' && object[ key ] === value ? true : false );
  }

}
