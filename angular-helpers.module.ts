import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthObjectFilterPipe } from './pipes/auth-object-filter/auth-object-filter.pipe';
import { ObjectFilterPipe } from './pipes/object-filter/object-filter.pipe';

@NgModule({
  declarations: [
    AuthObjectFilterPipe,
    ObjectFilterPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    AuthObjectFilterPipe,
    ObjectFilterPipe
  ],
})
export class AngularHelpersModule { }
